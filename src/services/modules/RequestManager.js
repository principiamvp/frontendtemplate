import axios from 'axios';

export default class RequestManager {

  constructor(){
    this.createInstance('http://localhost:80');
  }

  createInstance(baseURL){
    this._instace = axios.create({
      baseURL: baseURL,
    })
  }

  get instance(){
    return this._instace;
  }

  get(path, params){
    return this.instance.get(path, {params: params});
  }

  async post(path, data){
    return this.instance.post(path, data);
  }

  async put(path, data){
    return this.instance.put(path, data);
  }

  async delete(path, data){
    return this.instance.delete(path, data);
  }

}